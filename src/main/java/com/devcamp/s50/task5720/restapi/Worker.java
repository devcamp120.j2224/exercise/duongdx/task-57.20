package com.devcamp.s50.task5720.restapi;

public class Worker extends Person{
    private int salary ;

    public Worker(String name, int age, String gender, Address address) {
        super(name, age, gender, address);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("nguoi lam dang an");
    }

    public Worker(String name, int age, String gender, Address address, int salary) {
        super(name, age, gender, address);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    public String working(){
        return new String("nguoi lam dang lam viec");
        
    }
}
