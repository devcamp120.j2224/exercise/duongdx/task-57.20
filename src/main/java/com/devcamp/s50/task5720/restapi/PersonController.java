package com.devcamp.s50.task5720.restapi;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    // Danh sach list student
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent() {
        ArrayList<Student> students = new ArrayList<Student>();

        Subject subjectHoaDaiCuong = new Subject("Hoa Dai Cuong", 2, new Professor("Duong", 28, "male", new Address()));
        Subject VatLiDaiCuong = new Subject("Vat Ly Dai Cuong", 2, new Professor("Iron", 50, "male", new Address()));
        Subject HinhHocHoaHinh = new Subject("Hinh Hoc Hoa Hinh", 1, new Professor("ChauTinhTri", 55, "male", new Address()));
        Subject AnhVan = new Subject("Anh Van", 1, new Professor("CoGiaoThao", 45, "female", new Address()));
        Subject GiaiTich = new Subject("Giai Tich", 1, new Professor("MyNhan", 35, "female", new Address()));
        ArrayList<Subject> subjects1 = new ArrayList<>();
        ArrayList<Subject> subjects2 = new ArrayList<>();
        ArrayList<Subject> subjects3 = new ArrayList<>();
        subjects1.add(subjectHoaDaiCuong);
        subjects2.add(VatLiDaiCuong);
        subjects2.add(HinhHocHoaHinh);
        subjects3.add(AnhVan);
        subjects3.add(GiaiTich);
        Student studentPeter = new Student("Peter", 19, "male", new Address(), 1, subjects1);
        Student studentMary = new Student("Mary", 21, "male", new Address(), 2, subjects2);
        Student studentLinh = new Student("Linh", 22, "female", new Address(), 3, subjects3);
        
        students.add(studentPeter);
        students.add(studentMary);
        students.add(studentLinh);
        return students;
    }
}
