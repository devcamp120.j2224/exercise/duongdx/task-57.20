package com.devcamp.s50.task5720.restapi;

public class Professor extends Person{
    private int salary ;
    
    public Professor(String name, int age, String gender, Address address) {
        super(name, age, gender, address);
        //TODO Auto-generated constructor stub
    }

    public Professor(String name, int age, String gender, Address address, int salary) {
        super(name, age, gender, address);
        this.salary = salary;
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Giao Su Dang An");
    }
    
    public String teaching(){
        return new String("giao su day hoc");
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
