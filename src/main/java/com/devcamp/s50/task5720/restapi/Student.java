package com.devcamp.s50.task5720.restapi;

import java.util.ArrayList;

public class Student extends Person{
    private int studentid ;
    private ArrayList<Subject> listSubject ;

    public Student(String name, int age, String gender, Address address) {
        super(name, age, gender, address);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        
    }

    
    
    public Student(String name, int age, String gender, Address address, int studentid,
            ArrayList<Subject> listSubject) {
        super(name, age, gender, address);
        this.studentid = studentid;
        this.listSubject = listSubject;
    }

    public String doHomeWork(){
        return  new String("hoc sinh lam bai tap ve nha");
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public ArrayList<Subject> getListSubject() {
        return listSubject;
    }

    public void setListSubject(ArrayList<Subject> listSubject) {
        this.listSubject = listSubject;
    }
}
